import XCTest
@testable import TargetActClientSPM

final class TargetActClientSPMTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(TargetActClientSPM().text, "Hello, World!")
    }
}
