// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "TargetActClientSPM",
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "TargetActClientSPM",
            targets: ["TargetActClientSPM"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "TargetActClientSPM",
            dependencies: ["TargetActClient"]),
        .binaryTarget(
            name: "TargetActClient",
            url: "https://cdn-jp.gsecondscreen.com/static/ios/TargetActClient.xcframework.zip",
            checksum: "2a0136701e7b76ddbf509c019ed5ebab80a6827a2d429a02490cd132e32e8767"
        ),
        .testTarget(
            name: "TargetActClientSPMTests",
            dependencies: ["TargetActClientSPM"]),
    ]
)
